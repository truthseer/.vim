pushd ~/.vim
$already_staged = git status | grep "Changes to be committed:"

if ($already_staged) {
    echo "There are changes staged for a commit."
    echo "Please ether commit or unstage them to continue."
} else {
    echo Updating...
    git submodule foreach git pull origin master
    git add bundle/*
    $updated_bundle = git status | grep "Changes to be committed:"
    if ($updated_bundle) {
        git commit -m "update bundle"
    }
    echo Done.
}
popd
