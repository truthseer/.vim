set nocompatible " Don't need to be compatible with Vi.

" Start pathogen ('runtime' doesn't work?).
source ~/.vim/bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect() " Load anything in the ./bundle directory.
call pathogen#helptags() " Make any help files.

"------------------------------------------------
"| Anything else is in the ./settings directory |
"------------------------------------------------
