" When browsing a tree, make .. go up one node.
autocmd user fugitive
    \ if fugitive#buffer().type() =~# '^\%(tree\|blob\)$' |
    \     nnoremap <buffer> .. :edit %:h<CR> |
    \ endif

" When browsing a tree, prevent fugitive buffers from staying behind.
autocmd bufreadpost fugitive://* set bufhidden=delete
