" Resize windows with the arrow keys.
nnoremap <m-up> <c-w>+
nnoremap <m-down> <c-w>-
nnoremap <m-left> <c-w><
nnoremap <m-right> <c-w>>

" An easier way to split a window.
nnoremap <silent> <leader>ss <c-w>s
nnoremap <silent> <leader>vv <c-w>v

" Moving between windows.
nmap <c-j> <c-w>h
nmap <c-k> <C-w>j
nmap <c-l> <C-w>k
nmap <c-;> <C-w>l

" Moving windows around.
noremap \j <C-w>H
noremap \k <C-w>J
noremap \l <C-w>K
noremap \; <C-w>L
