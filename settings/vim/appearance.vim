" A nice working environment.
set background=dark
let g:solarized_italic=0 " Italics don't work quite right in Vim yet.
colorscheme solarized

" Show the Git branch in the status line.
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P
set ruler " Show the cursor coordinates in the status line.
set showcmd " Show normal mode commands as you are typing them.
set showmode " Show the mode you are currently in.
set wildmenu " Auto completion menu for the command line.
set guicursor=a:blinkon0 " Disable the blinking cursor.
set guitablabel=%N:\ %t\ %M " Show tab numbers.

set numberwidth=3 " Minimum number of columns that the line numbers take up.
" Always have 80 columns for the file.
let &columns=80 + &numberwidth " A mostly working replacement of the below.
if(has('gui_running'))
    set lines=42 " Height of the Vim window.
else
    set lines=37
endif



finish " The rest doesn't work yet!

let numberOfColumns=80
call FixColumns()

inoremap <cr> <cr><c-o>:call FixColumns()<cr>

autocmd! bufenter *
    \ let numberOfDigets=strlen(string(GetNumberOfLinesInBuffer()))

function! FixColumns()
    let newNumberOfDigets=strlen(string(GetNumberOfLinesInBuffer()))
    if newNumberOfDigets != g:numberOfDigets
        let numcol=g:numberOfColumns + newNumberOfDigets - g:numberOfDigets + 1
        let &columns=numcol
    endif
endfunction

function! GetNumberOfLinesInBuffer()
    let origpos=getpos(".")

    normal gg
    let startpos=getpos(".")[1] - 1
    normal G
    let endpos=getpos(".")[1]

    call setpos(".", origpos)
    return endpos - startpos
endfunction
