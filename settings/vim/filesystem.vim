" Enable file type detection.
filetype on

" Load specific plug-ins and indent based on file type.
filetype plugin indent on

" Reload (source) the current file.
nnoremap <silent> <leader>so :so %<cr>

" Insert current directory into the Vim command line.
cnoremap %% <c-r>=expand('%:h').'/'<cr>

" Easily open files in the same place as the current file in different ways.
map <leader>ew :e %%
map <leader>es :sp %%
map <leader>ev :vsp %%
map <leader>et :tabe %%

" Treat .rss and .atom files as XML.
autocmd bufnewfile,bufread *.rss,*.atom setfiletype xml

" Easily turn off the read only attribute of a file.
nnoremap <silent> <leader>nro :call MakeNotReadOnly()<cr>
function! MakeNotReadOnly()
    call DoWithNoFileChangedWarning("Shell attrib -R %")
    setlocal noreadonly
endfunction
