" Need to be able to toggle search highlighting at will
noremap <silent><leader>hl :set hlsearch! hlsearch?<CR>

" When searching, turn highlighting on
noremap <silent>/ :set hlsearch<CR>/
noremap <silent>? :set hlsearch<CR>?
noremap <silent># :set hlsearch<CR>#
noremap <silent>* :set hlsearch<CR>*

" A quick way to turn highlighting off when done searching
noremap <silent>// :set nohlsearch hlsearch?<CR>

set incsearch " Show matches while typing in a search expression.
