function! Preserve(cmd)
    " Save state
    let _s=@/ " Current search
    let pos = getpos(".") " Cursor position

    " Do command
    execute a:cmd

    " Restore state
    let @/=_s " Current search
    call setpos(".", pos) " Cursor position
endfunction

function! DoWithNoFileChangedWarning(cmd)
    " Set an environment variable to current buffer name.
    let $aucfile = expand("%")
    " This autocmd only applies to the current buffer and removes itself after
    " it runs one time.
    autocmd filechangedshell $aucfile autocmd! filechangedshell $aucfile

    let cmd = 'silent ' . a:cmd
    execute cmd
endfunction
