set autoindent " Auto indenting is good.
set smartindent " Indent based on the context of the programming language.
set smarttab " Use shiftwidth for the indenting amount.

" Default indentation (using tab): 4 spaces.
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" Languages that are fussy over tabs vs. spaces.
autocmd filetype make setlocal tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab
autocmd filetype yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab

" Make a new line and deindent it.
inoremap <s-return> <return><backspace>
