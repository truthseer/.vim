" Reposition the keys used.
noremap j h
noremap k j
noremap l k
noremap ; l

" Re expose lost a function.
noremap h ;

" Easy movement across wrapped lines.
noremap <m-k> gj
noremap <m-l> gk
noremap <m-$> g$
noremap <m-0> g0
noremap <m-^> g^

" You can continue to hold down the alt/meta key when going left and right.
noremap <m-j> h
noremap <m-;> l
