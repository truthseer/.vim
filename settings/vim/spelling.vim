" Spell checking is awesome.
setlocal spelllang=en_us
" Toggle spell checking.
nnoremap <silent> <leader>sc :set spell! spell?<cr>
" Spell file cleaner.
nnoremap <silent> <leader>csf :runtime spell/cleanadd.vim<cr>
