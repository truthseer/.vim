" Change <leader> from \ to , because it is easier to reach.
let mapleader=","
" Don't loose any functionality.
noremap - ,

" Shortcut to open the Vim settings directory.
nnoremap <leader>vrc :tabedit $home/.vim/settings/vim/<cr>
" Source changes to the Vim settings files.
autocmd! bufwritepost,filewritepost $home/.vim/settings/vim/*.vim source %

set history=50 " Keep a decent amount of history.
set winaltkeys=no " Use the mouse to open a menu!
set mouse=a " Make the mouse usable for those occasions when it is useful.
set formatoptions=crq " Allow comment formatting, auto-insert comment leader.

" Toggle 'keep current line in the center of the screen' mode
nnoremap <leader>C :let &scrolloff=999-(&scrolloff-4)<cr>
"set scrolloff=8 " Minimum space between the cursor and screen edges.

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gVim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd bufreadpost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \     execute "normal! g`\"" |
    \ endif

" Backspace will wrap to previous lines.
set backspace=indent,eol,start

" Disable the arrow keys, and backspace for movement.
noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
noremap <backspace> <nop>

" Don't want delete to act like the x key.
noremap <del> <nop>

" Close windows and buffers based on context.
cnoremap <silent> q<cr> call <sid>SmartClose()<cr>
function! <sid>SmartClose()
    let number_of_windows_to_this_buffer =
        \ len(filter(range(1, winnr('$')), "winbufnr(v:val) == bufnr('%')"))
    let number_of_listed_buffers =
        \ len(filter(range(1, bufnr('$')), 'buflisted(v:val)'))

    if number_of_windows_to_this_buffer > 1
        wincmd c " Close the current window.
    " For some reason buflisted('%') doesn't get the right buffer, but
    " bufnr('%') does; so we nest them.
    elseif number_of_listed_buffers == 1 && buflisted(bufnr('%'))
        q " Quit Vim
    else
        wv " Save the marks (among other things).
        bdelete " Unlist buffer
    endif
endfunction

" Expose the old functionality of q.
cnoremap Q<cr> q<cr>
cnoremap Q!<cr> q!<cr>

" A fast way to save and (smart) close the buffer.
map <c-z><c-z> :w<cr>:q<cr>
" A fast way to save the buffer and quit Vim (in case).
map <c-Z><c-Z> :w<cr>:Q<cr>
" For those ecasions that you can't use ctrl-z or ctrl-Z.
cmap wq<cr> w<cr>:call <sid>SmartClose()<cr>
cmap wQ<cr> w<cr>:q<cr>

" Don't want to have annoying bells (visual or not), so set the bell to be
" visual and then the visual queue to nothing.
set visualbell
" For some reason you need to use t_vb= in an autocmd.
autocmd vimenter * set t_vb=
