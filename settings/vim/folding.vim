set foldmethod=indent " Folding style I like.
set nofoldenable " Don't fold by default.

" Make sure folding is ALWAYS off by default when a file is opened.
"au filereadpost,bufreadpost * normal zn
"autocmd bufadd * normal zn

" Make navigating folds easier.
nnoremap <space> za
vnoremap <space> za
nnoremap <s-space> zc
