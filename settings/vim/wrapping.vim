" Toggles for line wrapping.
command! -nargs=* Wrap call WrapLines()
command! -nargs=* Nowrap call NoWrapLines()

function! WrapLines()
    set wrap linebreak nolist

    " Easy line navigation.
    noremap k gj
    noremap l gk
    noremap 0 g0
    noremap $ g$
endfunction

function! NoWrapLines()
    set nowrap nolinebreak

    " Reset to normal.
    noremap k j
    noremap l k
    noremap 0 0
    noremap $ $
endfunction

set linebreak " Wrap lines at better points than just the screen edge.
set textwidth=76 " Keep comments at a reasonable length.
