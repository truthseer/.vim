function! CleverTab()
    if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
        return "\<tab>"
    else
        return "\<c-n>"
    endif
endfunction

function! CleverTabReverse()
    if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction

inoremap <tab> <c-r>=CleverTab()<cr>
inoremap <s-tab> <c-r>=CleverTabReverse()<cr>
