" A way to strip trailing whitespace.
let stripTrailingWhitespace = "%s/\\s\\+$//e"
nnoremap <silent> _$ :call Preserve(stripTrailingWhitespace)<cr>
"autocmd bufwritepre *.py,*.js :call Preserve(stripTrailingWhitespace)<cr>

" Shortcut to toggle the visibility of invisible characters.
nmap <leader>shc :set list!<cr>

" Invisible characters to show.
set listchars=tab:>\ ,eol:�,trail:�

" Using the middle finger of your right hand you can type underscores with
" alt-k (add Shift to type dashes).
inoremap <silent> <m-k> _
inoremap <silent> <m-K> -

" Make Y consistent with C and D.
nnoremap Y y$

" Execute the line the cursor is on.
nnoremap <leader>vc yy:<c-f>p<cr>

" Bubble (move) single lines.
nmap <c-up> [e
nmap <c-down> ]e
" Bubble multiple lines.
vmap <c-up> [egVV
vmap <c-down> ]egVV

" Cut
vnoremap <c-x> "+x
" Copy
vnoremap <c-c> "+y
" Paste
noremap <c-v> "+gP

" Expose the original <c-v> functionality.
noremap <c-q> <c-v>

" Pasting block-wise and line-wise selections is not possible in Insert and
" Visual mode without the +virtualedit feature.  They are pasted as if they
" were character-wise instead.
" Uses the paste.vim autoload script.
execute 'inoremap <script> <c-v>' paste#paste_cmd['i']
execute 'vnoremap <script> <c-v>' paste#paste_cmd['v']

" Select all
noremap <c-a> ggVG
xnoremap <c-a> <c-c>ggVG
cnoremap <c-a> <c-f>V

" Don't need to use Ex mode, so use we can use Q for formatting.
noremap Q gq

" Yank inner word.
nnoremap <leader>yw yiww
" Overwrite inner word with the contents of the yank buffer.
nnoremap <leader>ow viwp

" Shortcuts to change the contents of various enclosures.
nnoremap <silent> <m-'> :call <sid>ChangeNextEnclosure("'", "'")<cr>
nnoremap <silent> <m-"> :call <sid>ChangeNextEnclosure('"', '"')<cr>
nnoremap <silent> <m-(> :call <sid>ChangeNextEnclosure("(", ")")<cr>
nnoremap <silent> <m-)> :call <sid>ChangeNextEnclosure("(", ")")<cr>
nnoremap <silent> <m-[> :call <sid>ChangeNextEnclosure("[", "]")<cr>
nnoremap <silent> <m-]> :call <sid>ChangeNextEnclosure("[", "]")<cr>
nnoremap <silent> <m-{> :call <sid>ChangeNextEnclosure("{", "}")<cr>
nnoremap <silent> <m-}> :call <sid>ChangeNextEnclosure("{", "}")<cr>

function! <sid>ChangeNextEnclosure(e1, e2)
    let l = line(".") " The line the cursor is on.
    let c = col(".") " The column the cursor is on.

    execute "normal f" . a:e1
    let c1 = col(".")

    call cursor(l, c) " Restore the cursor position.
    execute "normal f" . a:e2
    let c2 = col(".")

    if c != c1 && (c1 < c2 || c == c2)
        call cursor(l, c1)
        call feedkeys("ci" . a:e1)
    elseif c != c2
        call cursor(l, c2)
        call feedkeys("ci" . a:e2)
    else
        echo "There is nothing to change on this line."
    endif
endfunction
