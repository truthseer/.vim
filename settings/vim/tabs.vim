" Put all windows into tabs.
nnoremap <m-t> <c-w>T

" Move between tabs.
nnoremap <m-s-]> gt
nnoremap <m-s-[> gT

" Switch to tab number.
nnoremap <m-1> 1gt
nnoremap <m-2> 2gt
nnoremap <m-3> 3gt
nnoremap <m-4> 4gt
nnoremap <m-5> 5gt
nnoremap <m-6> 6gt
nnoremap <m-7> 7gt
nnoremap <m-8> 8gt
nnoremap <m-9> 9gt

" Go to last tab.
nnoremap <silent> <m-0> :tablast<cr>
