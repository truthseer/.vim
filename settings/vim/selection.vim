" Visually select the text that was last edited/pasted
nnoremap gV `[v`]

" Need to give access to the old function of gV
nnoremap gnV gV
