" Easily move between multiple buffers.
nnoremap <silent> <leader>zx :bprev<cr>
nnoremap <silent> <leader>xz :bnext<cr>

" Make Vim act like other editors by not forcing you to save a file before you
" can view/edit a different one.
set hidden
