" Block commenting of lines.
let blockCommentExpretion="s/^/##/e"
let unblockCommentExpretion="s/^##//e"
noremap <silent> <a-3> :call Preserve(blockCommentExpretion)<cr>
vnoremap <silent> <a-3> :call Preserve(blockCommentExpretion)<cr>gv
noremap <silent> <a-4> :call Preserve(unblockCommentExpretion)<cr>
vnoremap <silent> <a-4> :call Preserve(unblockCommentExpretion)<cr>gv
