set writebackup " Keep a swap file.
set nobackup " Don't keep backups (use a version control system ;)

" Try to put swap files in a place that is out of the way, but if that fails
" put them in the same directory as the file that's being edited.
set directory=~/.vim/.swapfiles,.
