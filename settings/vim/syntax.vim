syntax on " Turn syntax highlighting on.

" Show syntax highlighting groups for the word under the cursor
nnoremap <c-P> :HLT<cr>

"if exists('*HexHighlight')
nnoremap <leader>hex :call HexHighlight()<cr>
"endif
